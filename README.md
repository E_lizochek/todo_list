#ToDo list

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

##About

This simple resource gives users the opportunity for optimization the process of performing various kinds of tasks. 
The main functionality-recording and saving tasks to list, mark them after completion.
It's realized, using [react hooks](https://ru.reactjs.org/docs/hooks-reference.html).

##Project setup

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


