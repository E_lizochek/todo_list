import './App.scss';
import { ToDoForm } from './components/ToDoForm/TodoForm'

function App() {
  return (
    <div className="App">
      <h1> ToDo list</h1>
      <ToDoForm />
    </div>
  );
}

export default App;
