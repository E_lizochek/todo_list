import "../ToDoForm/ToDoForm.scss"
import { Input } from "../Input/Input"
import { Button } from "../Button/Button"
import { ToDoList } from "../ToDoList/ToDoList"
import { useState } from 'react'


export type Tasks = {
    id: string,
    task: string,
    complete: boolean | string,
}


export function ToDoForm() {
    const [userInput, setUserInput] = useState<string>('')
    const [tasks, setTasks] = useState<Tasks[]>([])


    const addTask = (userInput: string) => {
        if (userInput) {
            const newTask = {
                id: Math.floor(Math.random() * 1000).toString(36),
                task: userInput,
                complete: false
            }
            setTasks([...tasks, newTask])
        }
    }

    const handleChange = (element: string) => { setUserInput(element) }
    const handleSubmit = () => {
        addTask(userInput)
        setUserInput("")
    }
    return (
        <div className="todoform">
            <p> Available tasks: {tasks.length}</p>
            <div>
                <Input
                    type='text'
                    value={userInput}
                    placeholder="Input your text"
                    onChange={handleChange} />
                <Button
                    onClick={handleSubmit}
                    title="Add task" />
            </div>
            <ToDoList data={tasks} setTasks={setTasks} />
        </div>
    )
}