import '../Button/Button.scss'
import classNames from 'classnames'

type Props = {
  theme?: "filter" | "done" | "delete";
    onClick: () => void;
    title: string;
} 

export function Button(props: Props):JSX.Element {
  const ButtonClass = classNames(
    'button',
    { 'button__filter': props.theme === 'filter' },
    { 'button__done': props.theme === 'done' },
    {'button__delete': props.theme === 'delete'})


    return (
      <button 
      className={ButtonClass} 
      onClick={props.onClick} 
      >{props.title}</button>
    );
  }