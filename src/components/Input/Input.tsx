import '../Input/Input.scss'

type Props = {
    name?: string;
    type: 'text';
    placeholder?: string;
    id?: string;
    value?: string;
    onChange: (e:string) => void
}

export function Input(props: Props): JSX.Element {

    function handleChange(e: React.ChangeEvent<HTMLInputElement>): void {
        props.onChange(e.currentTarget.value)
    }

    return (
        <input 
        className='input' 
        name={props.name} 
        type={props.type} 
        placeholder={props.placeholder} 
        onChange ={handleChange} />
    )
}

